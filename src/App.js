import React, {Component} from 'react';
import './App.css';
import Score from './Team/Score.js';
import { isTSDeclareMethod } from '@babel/types';
// import Input from './Team/input.js';


class App extends Component {
  state = {
    teams_name: ["Hawks", "Eagles", "Lakers", "Raptors", "Rockets", "Warriors", "Bulls"],
    teams: [
      {name: "Hawks", id:"123"},
      {name: "Eagles", id:"456"}
    ],
    showScoreboard: false
  }

  toggleHandler = () => {  
    const doesShow = this.state.showScoreboard;
    this.setState({
      showScoreboard: !doesShow  
    })
    console.log(this.state.showScoreboard);
  }

  switchTeamByInput = (event, idteam) => {
    // ngambil 1 index dari array teams berdasar id
    const team_index = this.state.teams.findIndex(t => {
      return t.id === idteam
    })
    console.log(team_index);
    const teamBaru = {
      ...this.state.teams[team_index]
    };

    teamBaru.name = event.target.value;
    console.log(teamBaru.name)
    const teamsBaru = [...this.state.teams];
    teamsBaru[team_index] = teamBaru
    this.setState({
      teams: teamsBaru
    })
  }

  switchTeamButton = (idteam) => {
    const team_index = this.state.teams.findIndex(t => {
      return t.id === idteam
    })
    // const team_index = "0";
    console.log(team_index);
    const teamBaru = {
      ...this.state.teams[team_index]
    }

    teamBaru.name = this.state.teams_name[Math.floor(Math.random()* this.state.teams_name.length)]

    const teamsBaru = [...this.state.teams]
    teamsBaru[team_index] = teamBaru
    this.setState({
      teams:teamsBaru
    })
  }

  deleteButton = (teamsIndex) => {
    // const team_index = this.state.team.findIndex(t => {
    //   return t.id === idteam
  // })
    const teamsBaru = [...this.state.teams];
    teamsBaru.splice(teamsIndex, 1)
    this.setState({
      teams: teamsBaru
    })
  }
/////////////////////////////////////////////////////////
  // switchTeamA = ()=>{
  //   this.setState({
  //     teams:[
  //       {name: this.state.teams_name[Math.floor(Math.random() * this.state.teams_name.length)]},
  //       {name: this.state.teams[1].name}
  //     ]
  //   })
  // }

  // switchTeamB = () =>{
  //   this.setState({
  //     teams:[
  //       {name: this.state.teams[0].name},
  //       {name: this.state.teams_name[Math.floor(Math.random() * this.state.teams_name.length)]}
  //     ]
  //   })
  // }

  // inputTeamA = (event) => {
  //   // const value = event.target.value;
  //   this.setState({
  //     teams:[
  //       {name: event.target.value},
  //       {name: this.state.teams[1].name}
  //     ]
  //   });
  // }

  // inputTeamB = (event) => {
  //   // const value = event.target.value;
  //   this.setState({
  //     teams:[
  //       {name: this.state.teams[0].name},
  //       {name: event.target.value}
  //     ]
  //   });
  // }

  render(){
    let scores = null;
  
    if (this.state.showScoreboard) {
      scores =
        <div className="scoreboard"> 
          {this.state.teams.map((team,index) => {
            return <div className="team"> 
            <Score
            name={team.name}
            key={team.id}
            switchByInput={(event) => this.switchTeamByInput(event, team.id)}
            teamButton = {() => this.switchTeamButton(team.id)}
            deleteButton={this.deleteButton.bind(this, index)}
            />
            </div>
            }
          )}
        </div>
      }
    return (
    <div className="App">
      <h1>SCOREBOARD</h1>
      <button id="toggle" onClick={this.toggleHandler}>{this.state.showScoreboard? "HIDE" : "SHOW"}</button>
      <br/> <br/>
      {scores}
    </div>
    );
  }
}


export default App;

{/* <div className= "App"> 
      <div className="teamA">  
        <Score 
        name = {this.state.teams[0].name}
        switchByInput = {this.inputTeamA}
        />
        <br/><br/>
        <button className="teambtn" onClick={this.switchTeamA}>Switch Team</button>
      </div>
      <div className="teamB">  
        <Score 
        name={this.state.teams[1].name} 
        />
        <br/><br/>
        <button className="teambtn" onClick={this.switchTeamB}>Switch Team</button>
      </div>
    </div> */}




